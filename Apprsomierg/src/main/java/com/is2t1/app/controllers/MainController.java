/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.is2t1.app.controllers;

import com.is2t1.app.views.ContactFrame;
import com.is2t1.app.views.MainFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class MainController implements ActionListener{
    MainFrame mainFrame;

    public MainController(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        System.out.println(event.getActionCommand());
        switch(event.getActionCommand()){
            case "MostrarContactos":
                showContactFrame();
                break;
            case "Salir":
                System.exit(0);
                break;
        }     
    }
    
    public void showContactFrame(){
        ContactFrame c = new ContactFrame();
        mainFrame.showChild(c, false); 
    }
      
    
}
