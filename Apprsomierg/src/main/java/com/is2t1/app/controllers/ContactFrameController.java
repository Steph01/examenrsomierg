/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.is2t1.app.controllers;
 
import com.is2t1.app.models.Contact;
import com.is2t1.app.models.Email;
import com.is2t1.app.models.Phone;
import com.is2t1.app.views.ContactFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class ContactFrameController implements ActionListener{
    ContactFrame contactFrame;
    Contact contact;
    Phone p;
    Email e;
    JFileChooser d;

    public ContactFrameController(ContactFrame cf) {
        super();
        contactFrame = cf;
        contact = new Contact();
    }
    
    public void setContact(Contact b){
        contact = b;
    }
    public void setContact(String filePath){
        File f = new File(filePath);
        readContact(f);
    }
    public Contact getContact(){
        return contact;
    }     
       

    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "save":
                d.showSaveDialog(contactFrame); 
                contact = contactFrame.getContact();
                writeContact(d.getSelectedFile());
                break;
            case "select":                
                d.showOpenDialog(contactFrame);  
                contact= readContact(d.getSelectedFile());
                contactFrame.setContactData(contact,p,this.e);
                break;
            case "clear":
                contactFrame.clear();
                break;
               
            
        }
    }
    
    private void writeContact(File file){
         try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file)); 
            w.writeObject(getContact());
            w.flush();
        } catch (IOException ex) {
            Logger.getLogger(ContactFrameController.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
    
    private Contact readContact(File file){
         try{
            ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(file));
            return (Contact)ois.readObject();
        }
        catch(FileNotFoundException e){
            JOptionPane.showMessageDialog(contactFrame, e.getMessage(),contactFrame.getTitle(),JOptionPane.WARNING_MESSAGE);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(ContactFrameController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    
}
