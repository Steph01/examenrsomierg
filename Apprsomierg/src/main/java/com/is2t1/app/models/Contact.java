/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.is2t1.app.models;

import java.io.Serializable;

/**
 *
 * @author willj
 */
public class Contact implements Serializable{
    private String firstName;
    private String lastName;
    private String direccion;
    private Phone [] phones;
    private Email [] emails;
    private Address [] addresses;

    public Contact() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Phone[] getPhones() {
        return phones;
    }

    public void setPhones(Phone[] phones) {
        this.phones = phones;
    }

    public Email[] getEmails() {
        return emails;
    }

    public void setEmails(Email[] emails) {
        this.emails = emails;
    }

    public Address[] getAddresses() {
        return addresses;
    }

    public void setAddresses(Address[] addresses) {
        this.addresses = addresses;
    }
    
}
