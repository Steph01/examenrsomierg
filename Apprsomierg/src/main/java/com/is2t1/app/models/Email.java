/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.is2t1.app.models;

import java.io.Serializable;

/**
 *
 * @author willj
 */
public class Email implements Serializable{
    private String type;
    private String email;

    public Email() {
    }

    public Email(String type, String email) {
        this.type = type;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}
