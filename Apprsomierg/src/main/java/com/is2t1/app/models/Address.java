/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.is2t1.app.models;

import java.io.Serializable;

/**
 *
 * @author willj
 */
public class Address implements Serializable{
    private String type;
    private String address;

    public Address() {
    }

    public Address(String type, String address) {
        this.type = type;
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}
